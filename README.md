# 环境
- tensorflow 1.8
- numpy 
- gym
- pybox2d


# Deep Q Network
1. 使用说明：

执行`python lunar_lander.py [-d] [-D] [-r] [-l] [-t]`命令运行程序。

-d: 使用Dueling DQN

-D: 使用Double DQN

-r: 展示游戏画面

-l: 输出tensorboard log文件

-t: 训练模式

如果你想使用Dueling DQN 并且使用 Double DQN对LunarLander环境进行训练，执行

`python lunar_lander.py -d -D -t`

如果你想读取保存好的模型，展示游戏画面，执行

`python lunar_lander.py -d -D -r`

注意：如果训练的时候使用了Dueling DQN `-d`，则读取的时候记得也要带上 `-d`。

2. 结果：

![CartPole](https://gitee.com/kdldbq/reinforcement-learning/raw/master/deep-q-network/result/cart-pole.gif "CartPole")

![LunarLander](https://gitee.com/kdldbq/reinforcement-learning/raw/master/deep-q-network/result/lunar-lander.gif "LunarLander")


